package rw.viden.tenantsmanagement.models.entity

import jakarta.persistence.*
import jakarta.validation.constraints.Size
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Data
import lombok.NoArgsConstructor
import java.util.*

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Shard (
    @Id
    @Column(name = "id", nullable = false)
    val id: Int? = null,

    @Size(max = 30)
    @Column(name = "db") val db: String,

    @Column(name = "no_of_tenants")
    var numberOfTenants:Int = 0,

    @OneToMany(mappedBy = "shard", cascade = [CascadeType.ALL], orphanRemoval = true)
    @Builder.Default
    val tenants: MutableSet<Tenant> = HashSet()
){
    constructor() : this(db="")

    fun addTenant(tenant: Tenant) {
        tenants.add(tenant)
        numberOfTenants++
        tenant.shard=this
    }

    fun removeTenant(tenant: Tenant) {
        if (tenants.remove(tenant)) {
            numberOfTenants--
            tenant.shard=null
        } else {
            throw IllegalStateException("Tenant $tenant not found in shard")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val that = other as Shard
        return id != null && id == that.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }
}