package rw.viden.tenantsmanagement.models.entity

import jakarta.persistence.*
import jakarta.validation.constraints.Size
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Data
import lombok.NoArgsConstructor
import java.util.*


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Tenant (
    @Id
    @Size(max = 30)
    @Column(name = "tenant_id")
    val tenantId: String? = null,

    @ManyToOne(fetch = FetchType.LAZY)
    var shard: Shard? = null
){
    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val that = other as Tenant
        return tenantId != null && tenantId == that.tenantId
    }

    override fun hashCode(): Int {
        return Objects.hash(tenantId)
    }
}