package rw.viden.tenantsmanagement.routes

import lombok.RequiredArgsConstructor
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import rw.viden.tenantsmanagement.service.TenantManagementService

@RequiredArgsConstructor
@Controller
@RequestMapping("/")
class TenantsApiController(private var tenantManagementService: TenantManagementService) {

    @PostMapping("/tenants")
    fun createTenant(@RequestParam(required = true) tenantId: String): ResponseEntity<Void> {
        tenantManagementService.createTenant(tenantId)
        return ResponseEntity<Void>(HttpStatus.OK)
    }
}