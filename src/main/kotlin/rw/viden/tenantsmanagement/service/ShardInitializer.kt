package rw.viden.tenantsmanagement.service

import liquibase.exception.LiquibaseException
import liquibase.integration.spring.SpringLiquibase
import lombok.RequiredArgsConstructor
import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.core.io.ResourceLoader
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.StatementCallback
import org.springframework.jdbc.datasource.SingleConnectionDataSource
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import rw.viden.tenantsmanagement.exception.ShardCreationException
import rw.viden.tenantsmanagement.models.entity.Shard
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement
import javax.sql.DataSource

interface ShardInitializer {
    fun initializeShard(shard: Shard)
}


@RequiredArgsConstructor
@Slf4j
@Service
@EnableConfigurationProperties(
    LiquibaseProperties::class
)
class ShardInitializerImpl(private var jdbcTemplate: JdbcTemplate) : ShardInitializer {



    @Qualifier("shardLiquibaseProperties")
    @Autowired private lateinit var liquibaseProperties: LiquibaseProperties

    @Autowired private lateinit var resourceLoader: ResourceLoader

    @Value("\${multitenancy.shard.datasource.url-prefix}")
    private lateinit var urlPrefix: String

    @Value("\${multitenancy.master.datasource.username}")
    private lateinit var username: String

    @Value("\${multitenancy.master.datasource.password}")
    private lateinit var password: String

    private val log: Logger = LoggerFactory.getLogger(ShardInitializer::class.java)

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    override fun initializeShard(shard: Shard) {
        try {
            createDatabase(shard.db)
            log.info("Created new shard {}", shard.db)
        } catch (e: DataAccessException) {
            throw ShardCreationException("Error when creating db: " + shard.db, e)
        }
        try {
            DriverManager.getConnection(urlPrefix + shard.db, username, password).use { connection ->
                val shardDataSource: DataSource = SingleConnectionDataSource(connection, false)
                runLiquibase(shardDataSource)
                log.info("Initialized shard {}", shard.db)
            }
        } catch (e: SQLException) {
            throw ShardCreationException("Error when populating db: ", e)
        } catch (e: LiquibaseException) {
            throw ShardCreationException("Error when populating db: ", e)
        }
    }

    private fun createDatabase(db: String) {
        jdbcTemplate.execute(
            StatementCallback { stmt: Statement -> stmt.execute("CREATE DATABASE $db") } as StatementCallback<Boolean>)
        jdbcTemplate.execute(StatementCallback { stmt: Statement ->
            stmt
                .execute("GRANT ALL PRIVILEGES ON DATABASE $db TO $username")
        } as StatementCallback<Boolean>)
    }

    @Throws(LiquibaseException::class)
    private fun runLiquibase(dataSource: DataSource) {
        val liquibase: SpringLiquibase = getSpringLiquibase(dataSource)
        liquibase.afterPropertiesSet()
    }

    protected fun getSpringLiquibase(dataSource: DataSource?): SpringLiquibase {
        val liquibase = SpringLiquibase()
        liquibase.setResourceLoader(resourceLoader)
        liquibase.dataSource = dataSource
        liquibase.changeLog = liquibaseProperties!!.changeLog
        liquibase.contexts = liquibaseProperties.contexts
        liquibase.defaultSchema = liquibaseProperties.defaultSchema
        liquibase.liquibaseSchema = liquibaseProperties.liquibaseSchema
        liquibase.liquibaseTablespace = liquibaseProperties.liquibaseTablespace
        liquibase.databaseChangeLogTable = liquibaseProperties.databaseChangeLogTable
        liquibase.databaseChangeLogLockTable = liquibaseProperties.databaseChangeLogLockTable
        liquibase.isDropFirst = liquibaseProperties.isDropFirst
        liquibase.setShouldRun(liquibaseProperties.isEnabled)
        liquibase.labelFilter = liquibaseProperties.labelFilter
        liquibase.setChangeLogParameters(liquibaseProperties.parameters)
        liquibase.setRollbackFile(liquibaseProperties.rollbackFile)
        liquibase.isTestRollbackOnUpdate = liquibaseProperties.isTestRollbackOnUpdate
        return liquibase
    }
}