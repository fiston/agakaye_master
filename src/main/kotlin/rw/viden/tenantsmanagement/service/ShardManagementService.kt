package rw.viden.tenantsmanagement.service

import lombok.RequiredArgsConstructor
import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rw.viden.tenantsmanagement.models.entity.Shard
import rw.viden.tenantsmanagement.models.entity.Tenant
import rw.viden.tenantsmanagement.repository.ShardRepository

interface ShardManagementService {
    /**
     * Allocate a tenant to a shard, creating a new shard if necessary.
     *
     * @param tenant the tenant, which must be a JPA managed object that
     * will be modified by the operation.
     */
    fun allocateToShard(tenant: Tenant)
}


@RequiredArgsConstructor
@Slf4j
@Service
class ShardManagementServiceImpl(
    private var shardRepository: ShardRepository,
    private var shardInitializer: ShardInitializer
) : ShardManagementService {


    @Value("\${multitenancy.master.database}")
    private lateinit var database: String

    @Value("\${multitenancy.shard.max-tenants}")
    private var maxTenants: Int = 0

    private val log: Logger = LoggerFactory.getLogger(ShardManagementService::class.java)

    @Transactional
    override fun allocateToShard(tenant: Tenant) {
        val shardsWithFreeCapacity: List<Shard> = shardRepository.findShardsWithFreeCapacity(maxTenants)
        if (shardsWithFreeCapacity.isNotEmpty()) {
            val shard: Shard = shardsWithFreeCapacity[0]
            shard.addTenant(tenant)
            log.info("Allocated tenant {} to shard {}", tenant.tenantId, shard.db)
        } else {
            val newShardIndex = shardRepository.count() as Int + 1
            val newShardName = database + DATABASE_NAME_INFIX + newShardIndex
            val shard: Shard = Shard(
                id=newShardIndex,
                db=newShardName)
            shardInitializer.initializeShard(shard)
            shard.addTenant(tenant)
            shardRepository.save(shard)
            log.info(
                "Allocated tenant {} to new shard {}",
                tenant.tenantId,
                shard.db
            )
        }
    }

    companion object {
        private const val DATABASE_NAME_INFIX = "_shard_"
    }
}
