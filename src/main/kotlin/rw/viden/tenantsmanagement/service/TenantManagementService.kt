package rw.viden.tenantsmanagement.service

import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rw.viden.tenantsmanagement.models.entity.Tenant

interface TenantManagementService {
    /**
     * Create a tenant and allocate to a suitable shard.
     */
    fun createTenant(tenantId: String)
}


@RequiredArgsConstructor
@Service
class TenantManagementServiceImpl(private var shardManagementService: ShardManagementService) :
    TenantManagementService {


    @Transactional
    override fun createTenant(tenantId: String) {
        val tenant = Tenant(tenantId = tenantId)

        shardManagementService.allocateToShard(tenant)
    }
}
