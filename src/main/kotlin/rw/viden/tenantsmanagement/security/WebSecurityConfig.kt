package rw.viden.tenantsmanagement.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.AuthenticationException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono


@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class WebSecurityConfiguration {

    private val logger: Logger = LoggerFactory.getLogger(WebSecurityConfiguration::class.java)

    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity
    ): SecurityWebFilterChain {
        return http
            .exceptionHandling()
            .authenticationEntryPoint { serverWebExchange: ServerWebExchange, exception: AuthenticationException ->
                Mono.fromRunnable {
                    serverWebExchange.response.statusCode = HttpStatus.UNAUTHORIZED
                    logger.error(exception.localizedMessage+" Ahambere")
                }
            }.accessDeniedHandler { serverWebExchange: ServerWebExchange, exception: AccessDeniedException ->
                Mono.fromRunnable {
                    serverWebExchange.response.statusCode = HttpStatus.FORBIDDEN
                    logger.error(exception.localizedMessage+" Ahandi "+ serverWebExchange.request.headers["Authorization"])
                }
            }.and()
            .authorizeExchange()
            .pathMatchers(HttpMethod.POST).permitAll()
            .pathMatchers("/**").permitAll()
            .pathMatchers("/signup").permitAll()
            .pathMatchers("/logout").permitAll()
            .pathMatchers("/verify_passcode").permitAll()
            .pathMatchers("/actuator/**").permitAll()
            .pathMatchers("/actuator/info").permitAll()
            .pathMatchers("/actuator/health").permitAll()
            .pathMatchers("/",
                "/v3/api-docs/**",
                "/swagger-ui.html",
                "/webjars/**",
                "/messaging/v3/api-docs",
                "/all-users"
            ).permitAll()
//            .anyExchange().authenticated()
//            .and()
//            .authorizeExchange()
            .pathMatchers("/who")
            .hasAnyRole("USER","ADMIN")
            .pathMatchers("/rubanda/**")
            .hasAnyRole("USER","ADMIN")
            .pathMatchers("/admin/**")
            .hasRole("ADMIN")
            .and()
            .csrf().disable()
            .formLogin().disable()
            .httpBasic().disable()
            .build()
    }


    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

}