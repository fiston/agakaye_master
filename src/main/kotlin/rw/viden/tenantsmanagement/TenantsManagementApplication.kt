package rw.viden.tenantsmanagement

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class, LiquibaseAutoConfiguration::class, R2dbcAutoConfiguration::class])
class TenantsManagementApplication

fun main(args: Array<String>) {
    runApplication<TenantsManagementApplication>(*args)
}
