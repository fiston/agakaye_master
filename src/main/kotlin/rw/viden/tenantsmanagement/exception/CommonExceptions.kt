package rw.viden.tenantsmanagement.exception

class ShardCreationException(message: String?, cause: Throwable?) : RuntimeException(message, cause)

