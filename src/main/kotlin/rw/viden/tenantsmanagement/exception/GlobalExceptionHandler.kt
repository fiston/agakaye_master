package rw.viden.tenantsmanagement.exception


import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.util.*


@Configuration
@Order(-2)
class GlobalExceptionHandler(private val objectMapper: ObjectMapper) : ErrorWebExceptionHandler {

    override fun handle(serverWebExchange: ServerWebExchange, throwable: Throwable): Mono<Void> {

        val bufferFactory = serverWebExchange.response.bufferFactory()
        when (throwable) {

            is HttpMessageNotReadableException -> {
                serverWebExchange.response.statusCode = HttpStatus.BAD_REQUEST
            }

            is IllegalAccessException -> {
                serverWebExchange.response.statusCode = HttpStatus.BAD_REQUEST
            }

            is IllegalArgumentException -> {
                serverWebExchange.response.statusCode = HttpStatus.BAD_REQUEST
            }

            is DataIntegrityViolationException -> {
                serverWebExchange.response.statusCode = HttpStatus.FORBIDDEN
            }

            is NoSuchElementException -> {
                serverWebExchange.response.statusCode = HttpStatus.NOT_FOUND

            }

            is MethodArgumentTypeMismatchException -> {
                serverWebExchange.response.statusCode = HttpStatus.NOT_FOUND

            }

            is EmptyResultDataAccessException -> {
                serverWebExchange.response.statusCode = HttpStatus.NOT_FOUND

            }

            is ShardCreationException -> {
                serverWebExchange.response.statusCode = HttpStatus.UNAUTHORIZED
            }

            else -> {
                serverWebExchange.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
            }
        }

        val dataBuffer: DataBuffer = try {
            bufferFactory.wrap(
                objectMapper.writeValueAsBytes(
                    HttpError(
                        status = serverWebExchange.response.statusCode!!.value(),
                        message = throwable.message.orEmpty(),
                        error = serverWebExchange.response.statusCode.toString(),
                        timestamps = Date().toString()
                    )
                )
            )
        } catch (e: JsonProcessingException) {
            bufferFactory.wrap("This should never be displayed, if it does, BADA-BOOM".toByteArray())
        }
        serverWebExchange.response.headers.contentType = MediaType.APPLICATION_JSON
        return serverWebExchange.response.writeWith(Mono.just(dataBuffer))
    }

    inner class HttpError internal constructor(
        val status: Int,
        val error: String,
        val message: String,
        val timestamps: String
    )
}