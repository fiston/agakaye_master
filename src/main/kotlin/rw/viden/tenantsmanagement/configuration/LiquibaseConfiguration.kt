package rw.viden.tenantsmanagement.configuration

import liquibase.integration.spring.SpringLiquibase
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource


@Configuration
@ConditionalOnProperty(name = ["multitenancy.master.liquibase.enabled"], havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties(
    LiquibaseProperties::class
)
class LiquibaseConfiguration {
    @Bean
    @ConfigurationProperties("multitenancy.master.liquibase")
    fun masterLiquibaseProperties(): LiquibaseProperties {
        return LiquibaseProperties()
    }

    @Bean
    @ConfigurationProperties("multitenancy.shard.liquibase")
    fun shardLiquibaseProperties(): LiquibaseProperties {
        return LiquibaseProperties()
    }

    @Bean
    fun masterLiquibase(@LiquibaseDataSource liquibaseDataSource: ObjectProvider<DataSource?>): SpringLiquibase {
        val liquibaseProperties = masterLiquibaseProperties()
        val liquibase = SpringLiquibase()
        liquibase.dataSource = liquibaseDataSource.ifAvailable
        liquibase.changeLog = liquibaseProperties.changeLog
        liquibase.contexts = liquibaseProperties.contexts
        liquibase.defaultSchema = liquibaseProperties.defaultSchema
        liquibase.liquibaseSchema = liquibaseProperties.liquibaseSchema
        liquibase.liquibaseTablespace = liquibaseProperties.liquibaseTablespace
        liquibase.databaseChangeLogTable = liquibaseProperties.databaseChangeLogTable
        liquibase.databaseChangeLogLockTable = liquibaseProperties.databaseChangeLogLockTable
        liquibase.isDropFirst = liquibaseProperties.isDropFirst
        liquibase.setShouldRun(liquibaseProperties.isEnabled)
        liquibase.labelFilter = liquibaseProperties.labelFilter
        liquibase.setChangeLogParameters(liquibaseProperties.parameters)
        liquibase.setRollbackFile(liquibaseProperties.rollbackFile)
        liquibase.isTestRollbackOnUpdate = liquibaseProperties.isTestRollbackOnUpdate
        return liquibase
    }
}