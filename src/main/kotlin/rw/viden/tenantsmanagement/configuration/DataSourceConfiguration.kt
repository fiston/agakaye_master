package rw.viden.tenantsmanagement.configuration

import com.zaxxer.hikari.HikariDataSource
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import javax.sql.DataSource

@Component
@Configuration
class DataSourceConfiguration {
    @Bean
    @ConfigurationProperties("multitenancy.master.datasource")
    fun masterDataSourceProperties(): DataSourceProperties {
        return DataSourceProperties()
    }

    @Bean
    @LiquibaseDataSource
    @ConfigurationProperties("multitenancy.master.datasource.hikari")
    fun masterDataSource(): DataSource {
        val dataSource = masterDataSourceProperties()
            .initializeDataSourceBuilder()
            .type(HikariDataSource::class.java)
            .build()
        dataSource.poolName = "masterDataSource"
        return dataSource
    }
}