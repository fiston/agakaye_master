package rw.viden.tenantsmanagement.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import rw.viden.tenantsmanagement.models.entity.Shard
import java.util.*

interface ShardRepository : JpaRepository<Shard, UUID> {

    @Query("SELECT s FROM Shard s WHERE s.numberOfTenants < :maxTenants")
    fun findShardsWithFreeCapacity(maxTenants: Int): List<Shard>

}